from typing import Protocol
from django.shortcuts import render
from .forms import UserRegister
from django.http import HttpResponseRedirect, HttpResponse

# Create your views here.

def thankyou(request):
    return render(request, 'success.html')

def show_form_data(request):
    if request.method  == 'POST':
        fm = UserRegister(request.POST)
        if fm.is_valid():
            print('post Validation')
            name = fm.cleaned_data['name']
            email = fm.cleaned_data['email']
            password = fm.cleaned_data['password']
            confirm_password = fm.cleaned_data['confirm_password']
            print(name)
            print(email)
            print(password)
            print(confirm_password)
            #page show on same previous urls 
            #return render(request, 'success.html', {'name': name})
            #page shoew on diffrent url use httpResponse
            return HttpResponseRedirect('/register/success')
           
    else:
        fm = UserRegister()
        print('else Validation')
    return render(request, 'userregistration.html', {'form': fm})
    #return HttpResponse(request, 'userregistration.html')
   
        