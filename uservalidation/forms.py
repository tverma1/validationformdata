from django import forms
from django .core import validators

class UserRegister(forms.Form):
    name = forms.CharField()
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput)
    confirm_password = forms.CharField(widget = forms.PasswordInput)
    
    '''
    # single field validation
    #in clean_name, name is the field name
    def clean_name(self):
        val_name = self.cleaned_data['name']
        if len(val_name) < 4:
            raise forms.ValidationError('Enter name  more than or equal 4 characters')
        return val_name'''
    
    #complete form validation
    def clean(self):
        cleaned_data = super().clean()
        
        name = self.cleaned_data['name']
        val_email = self.cleaned_data['email']
        val_pwd_1 = self.cleaned_data['password']
        val_pwd_2 = self.cleaned_data['confirm_password']
        
        if len(name) < 4:
            raise forms.ValidationError('Name  more than or equal 4 characters')
        
        if len(val_email) < 8:
            raise forms.ValidationError('Email  more than or equal 8 characters')
        
        if len(val_pwd_1) < 8:
            raise forms.ValidationError('password  more than or equal  8 characters')
        
        if val_pwd_1 != val_pwd_2:
            raise forms.ValidationError('password does not match')
       
           
        '''def clean(self):
            if 'password' in self.cleaned_data and 'password1' in self.cleaned_data and self.cleaned_data['password'] != self.cleaned_data['password1']:
                raise forms.ValidationError("The password does not match ")
            return self.cleaned_data'''